DESTDIR   =
prefix    = /usr
sbindir   = $(prefix)/sbin
sysconfdir= /etc
pkgsysconfdir= $(sysconfdir)/shutdown-at-night

# Install scripts in lib/, to make it trivial to rewrite the internal tools
# in any language, even compiled ones, without having to update the path used
# by the callers.
pkglibdir = $(prefix)/lib/shutdown-at-night

INSTALL = install

all:

install:
	$(INSTALL) -d $(DESTDIR)$(pkglibdir)/. $(DESTDIR)$(pkgsysconfdir)/.
	$(INSTALL) shutdown-at-night $(DESTDIR)$(pkglibdir)/.
	$(INSTALL) wakeupclients $(DESTDIR)$(pkglibdir)/.

distclean: clean
clean:
