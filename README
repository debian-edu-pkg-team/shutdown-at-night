Saving power by turning off clients at night
============================================

It is possible to save power by turning off clients at night, and turn
them automatically on in the morning.

There are some considerations to make when doing this:

 - The clients should not be shut down when someone is using them.
   This is done by checking the output from 'who', and as a special
   case, checking for the ssh connection command to work with 'bare'
   and 'display' type X2Go thin clients.

 - To avoid breaking fuses, it is a good idea to make sure all clients
   do not start at the same time.

 - There are three different methods available to wake up clients.
   Two use a BIOS feature and require a working and correct hardware
   clock, one of it in addition a motherboard and BIOS version
   supported by nvram-wakeup. The other requires a server with
   knowledge about all the clients to wake up and for all the clients
   to have support for wake-on-lan.

How to set it up
----------------

On clients that should turn off at night, touch
/etc/shutdown-at-night/shutdown-at-night, or add the hostname (ie the
output from 'uname -n' or 'hostname -s' on the client) to the netgroup
"shutdown-at-night-hosts".  The clients might need to have wake-on-lan
configured in the BIOS.  It is also important that the switches and
routers used between the wake-on-lan server and the clients will pass
the WOL packages to the clients even if the clients are turned off.
Some switches fail to pass on packages to clients that are missing in
the ARP table on the switch, and this block the WOL packages.

To enable wake-on-lan on the server, add the clients to
/etc/shutdown-at-night/clients, with one line per client, IP address
first, and MAC address (ethernet address) next, with space between
them, or create a script /etc/shutdown-at-night/clients-generator to
generate the list of clients on the fly.

Here is an example /etc/shutdown-at-night/clients-generator for use
with sitesummary:

  #!/bin/sh
  PATH=/usr/sbin:$PATH
  export PATH
  sitesummary-nodes -w

An alternative if the netgroup is used to activate shutdown-at-night
on clients is this script using the netgroup tool from the ng-utils
package:

  #!/bin/sh
  PATH=/usr/sbin:$PATH
  export PATH
  netgroup -h shutdown-at-night-hosts

This is an example for a /etc/default/shutdown-at-night
configuration file, if wakeup should be configured without using a
server.

  # These are the defaults for standalone usage for wakeup. Set from
  # "no" to "yes" to enable a wake method.
  WAKEUPTIME="07:00"
  # nvram-wake works only for special mother boards.
  NVRAM_WAKE_ENABLED="no"
  # Using the real time clock wakealarm feature should work better.
  ACPI_WAKE_ENABLED="no"


Blocking NVRAM/RTC wakeup of the machine itself
-----------------------------------------------

You can prevent the machine from preparing its own wake-up via NVRAM
and/or RTC alarm by putting creating an empty file of the name

  /etc/shutdown-at-night/shutdown-at-night-nowakeup-self


Blocking WoL wakeup of other machines
-------------------------------------

If you (temporarily) want to disable sending WoL packets to other
machines in the morning, you can block this by creating an empty file of
the name

  /etc/shutdown-at-night/shutdown-at-night-nowakeup-others


License
-------

This package is licensed using GNU General Public License v2 or later.

Bugreporting
------------

Report issues to the debian-edu@lists.debian.org mailing list.
