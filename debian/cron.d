# /etc/cron.d/shutdown-at-night

SHELL=/bin/sh
PATH=/sbin:/bin:/usr/sbin:/usr/bin

# Shutdown clients
0 16,17,18,19,20,21,22,23,0,1,2,3,4,5,6 * * * root test -x /usr/lib/shutdown-at-night/shutdown-at-night && /usr/lib/shutdown-at-night/shutdown-at-night

# Try to wake up clients on work days
30 6 * * 1,2,3,4,5 root test -x /usr/lib/shutdown-at-night/wakeupclients && /usr/lib/shutdown-at-night/wakeupclients
